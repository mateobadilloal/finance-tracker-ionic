angular.module('financeTracker.controllers', [])

.controller('AppCtrl', function ($scope, $ionicModal, $timeout) {})

.controller('homeController', function ($scope) {

})

.controller('currencyController', ['$scope', 'currencyFactory', '$timeout', function ($scope, currencyFactory, $timeout) {

        var cunrrenciesNamedArray = [];
        $scope.currencies = [];

        //Array of currencies that are available to convert
        $scope.coins = [
            "Choose Currency",
            "USD",
            "AUD",
            "BGN",
            "BRL",
            "CAD",
            "CHF",
            "CNY",
            "CZK",
            "DKK",
            "GBP",
            "HKD",
            "HRK",
            "HUF",
            "IDR",
            "ILS",
            "INR",
            "JPY",
            "KRW",
            "MXN",
            "MYR",
            "NOK",
            "NZD",
            "PHP",
            "PLN",
            "RON",
            "RUB",
            "SEK",
            "SGD",
            "THB",
            "TRY",
            "ZAR",
            "EUR"
        ];

        $scope.currency = {
            fromAmount: 0,
            toAmount: 0
        };

        //Every time the view is loaded it gets the name of the popular currencies from the database with getNamedCurs() then it goes to the fixer.io API to get the rates between the currencies with the methos fixerCurs()
        $scope.$on('$ionicView.enter', function (e) {
            cunrrenciesNamedArray = [];
            $scope.currencies = [];
            currencyFactory.getNamedCurs().query().
            $promise.then(
                function (response) {

                    for (var i = 0; i < response.length; i++) {
                        cunrrenciesNamedArray.push(response[i]);
                    }
                },
                function (response) {
                    console.log(response);
                });

            $timeout(function () {

                for (var i = 0; i < cunrrenciesNamedArray.length; i++) {
                    currencyFactory.fixerCurs().get({
                        origin: cunrrenciesNamedArray[i].origin,
                        versus: cunrrenciesNamedArray[i].versus
                    }).$promise.then(
                        function (response) {
                            var name = Object.keys(response.rates)[0] + '/' + response.base;
                            $scope.currencies.push({
                                name: name,
                                rates: response.rates[Object.keys(response.rates)[0]]
                            });

                        },
                        function (response) {
                            console.log(response);
                        }
                    );
                }
            }, 500);

        });


        //This method converts from one currency to another using the fixer.io API
        $scope.convert = function () {
            currencyFactory.fixerCurs().get({
                origin: $scope.currency.from,
                versus: $scope.currency.to
            }).$promise.then(
                function (response) {
                    var rate = response.rates[Object.keys(response.rates)[0]];
                    $scope.currency.toAmount = $scope.currency.fromAmount * rate;
                },
                function (response) {
                    console.log(response);
                }
            );

        };






    }])
    .controller('glossaryController', ['$scope', 'glossaryFactory', function ($scope, glossaryFactory) {

        //This controller gets the tips from the database to be shown in the financial glossary view
        $scope.tips = [];
        $scope.formula = '';
        glossaryFactory.getTips().query()
            .$promise.then(
                function (response) {
                    $scope.tips = response;
                    $scope.formula = $scope.tips[5].description;
                },
                function (response) {
                    console.log(response);
                }
            );


    }]);
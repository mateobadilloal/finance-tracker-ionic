
//katex library is in charge of the mathematical notation display in the financial glossary view,
angular.module('financeTracker', ['ionic', 'financeTracker.controllers', 'financeTracker.services', 'katex'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
// APPs routes
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sidebar.html',
    controller: 'AppCtrl'
  })
      .state('app.home', {
      url: '/home',
      views: {
          'menuContent': {
              templateUrl: 'templates/home.html',
              controller: 'homeController'
          }
      }
  })

  .state('app.glossary', {
    url: '/glossary',
    views: {
      'menuContent': {
        templateUrl: 'templates/glossary.html',
          controller: 'glossaryController'
      }
    }
  })

  .state('app.currency', {
      url: '/currency',
      views: {
        'menuContent': {
          templateUrl: 'templates/currency.html',
            controller: 'currencyController'
        }
      }
    })
   
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});

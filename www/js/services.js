'use strict';

angular.module('financeTracker.services', ['ngResource'])
    .constant("baseURL", "https://finance-tracker-back.herokuapp.com/api/")
    .constant("fixer", "https://api.fixer.io/latest?")



    .factory('glossaryFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        var glosFact = {};

        //Gets the tips from the database
        glosFact.getTips = function () {
            return $resource(baseURL + "tips", null, {
                'get': {
                    method: 'GET'
                }
            });
        };
        return glosFact;
    }])

    .factory('currencyFactory', ['$resource', 'baseURL', 'fixer', function ($resource, baseURL, fixer) {

        var cureFact = {};

        //Gets the currencies from the database to be shown in the popular currencies title
        cureFact.getNamedCurs = function () {
            return $resource(baseURL + "currencies  ", null, {
                'get': {
                    method: 'GET'
                }
            });
        };
        // Returns the rate between two currencies
        cureFact.fixerCurs = function () {
            return $resource(fixer + "base=:origin&symbols=:versus", null, {
                'get': {
                    method: 'GET',
                    params: {
                        origin: '@origin',
                        versus: '@versus'
                    }
                }
            });
        };
        return cureFact;
    }])
;